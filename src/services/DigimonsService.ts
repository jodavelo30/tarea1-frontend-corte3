import { DigimonI } from "../interfaces/DigimonInterfaces";
import { MonsterTypeI } from "../interfaces/MonsterTypeI";
const db = require('../db/Digimons.json');

module DigimonsService { 
    export function getAll(): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        return digimons
    }

    export function getById(id: number): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.id === id);
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon[0];
    }
    
    export function getByName(name: string): DigimonI[] {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.name.toLowerCase().includes(name.toLowerCase()));
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon;
    }

    export function add(digimon: DigimonI): DigimonI[] {
        const digimonsFull: Array<DigimonI> = db;
        //const s: Array<DigimonI> = digimons.filter(e => e.name.toLowerCase().includes(name.toLowerCase()));
        let exists = digimonsFull.filter(d => d.id === digimon.id).length > 0;
        if(exists){
            throw "El id ya existe";
        }
        digimonsFull.push(digimon);
        return digimonsFull;
    }

    export function getByType(type: string): DigimonI[] {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = [];
        digimons.forEach(d=>{
            d.type.forEach(t=>{
                if(t.name.toLowerCase().includes(type.toLowerCase())){
                    digimon.push(d);
                }
            });
        });

        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon;
    }

    export function against(idOne: number, idTwo: number): string{
        const digimons: Array<DigimonI> = db;
        const digimonOne: DigimonI = getById(idOne);
        const digimonTwo: DigimonI = getById(idTwo);

        const against = digimonOne.type.map(t=>t.name);

        //TODO
        return "";
    }

}

export default DigimonsService;