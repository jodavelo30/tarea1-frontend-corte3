import { SimplifiedTypeI } from "./SimplifiedTypeI";

export interface MonsterTypeI{
    id: number;
    name: string;
    immunes: Array<SimplifiedTypeI>;
    strengths: Array<SimplifiedTypeI>;
    weaknesses: Array<SimplifiedTypeI>;
}
