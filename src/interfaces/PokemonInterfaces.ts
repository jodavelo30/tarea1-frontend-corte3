import { SimplifiedTypeI } from "./SimplifiedTypeI";

export interface PokemonI {
    id: number;
    name: string;
    type: Array<SimplifiedTypeI>;
    img?: string;
}
